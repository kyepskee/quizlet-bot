(ns quizlet-bot.core
  (:gen-class)
  [:require [etaoin.keys :as k]]
  [:use [etaoin.api]]
  [:use [clojure.pprint]])

(def debug? true)

(def mobile-useragent "Mozilla/5.0 (Android 7.0; Mobile; rv:54.0) Gecko/54.0 Firefox/54.0")
(defonce driver (firefox {:prefs {"general.useragent.override" mobile-useragent}}))

(defn setup []
  ;; TODO: set cookie
  (set-window-size driver 500 1000))

(defonce vocabulary (atom []))

(defn make-term [text1 text2]
  [text1 text2])

(defn lookup-term [text]
  (some #(if (or (= (first %) text)
                 (= (second %) text))
           %
           nil)
        @vocabulary))
(defn lookup-matching-text [text]
  (some #(cond (= (first %) text) (second %)
               (= (second %) text) (first %)
               :else nil)
        @vocabulary))
;;
(defn contains-term-any? [term]
  (or (lookup-term (first term))
      (lookup-term (second term))))

;; FIXME: doesn't override old terms if incompatible
(defn add-term [term]
  (swap! vocabulary
         #(if (contains-term-any? term)
            %
            (conj % term))))

(defn make-card [text el]
  [text el])
(defn text-card [card]
  (first card))
(defn el-card [card]
  (second card))
(defn matching-card? [card text]
  (= (text-card card) text))
(defn match-cards [cards]
  (defn iter [cards non-matching-cards pairs]
    (if (empty? cards)
      [pairs non-matching-cards]
      (let* [current-card (first cards)
             text (text-card current-card)
             matching-text (lookup-matching-text text)
             matching-card (some #(if (matching-card? % matching-text)
                                    %
                                    nil)
                                 cards)]
        (if matching-card
          (iter (remove #(or (= % matching-card) (= % current-card))
                         cards)
                non-matching-cards
                (conj pairs [current-card matching-card]))
          (iter (rest cards)
                (conj non-matching-cards current-card)
                pairs)))))


  (iter cards [] []))

;; (defn stale? [el]
;;   (execute {:driver driver
;;             :method :get
;;             :p
;;             :data nil
;;             :}))
(defn get-cards []
  ;; (defn get-card-text [subcontainer]
  ;;   (children))
  (let [card-container-xpath "/html/body/div[3]/main/div[3]/div/div/div/div[2]/div/div"]
    (wait-exists driver card-container-xpath)
    (let* [card-container (query driver card-container-xpath)
           cs (children driver card-container "div/div/div/div")
           cards (map #(make-card (get-element-text-el driver %) %)
                      cs)]
      ;; (doseq [card cards]
      ;;   (get-element-))
      cards)))

(defn match [card-pairs]
  (doseq [pair card-pairs]
    (click-el driver (el-card (first pair)))
    (click-el driver (el-card (second pair)))))

(defn learn [non-matching-cards]
  ;; FIXME: this is hacky
  (defn matched? [card]
    (wait driver 0.5)
    (try
      (not (enabled-el? driver (el-card card)))
      (catch Exception _ true)))
  (defn iter [non-matching-cards not-checked]
    (let [card1 (first non-matching-cards)
          card2 (first not-checked)]
      (println "tried matching: " (text-card card1) (text-card card2))
      (doto driver
        (click-el (el-card card1))
        (click-el (el-card card2)))
      (if (matched? card1)
        (let [new-cards (remove #(or (= % card1) (= % card2))
                                non-matching-cards)]
          (add-term (make-term (text-card card1)
                               (text-card card2)))
          (println "MATCHED---")
          (println (text-card card1) (text-card card2))
          (pprint @vocabulary)
          (pprint new-cards)
          (println)
          (if (empty? new-cards)
            nil
            (iter new-cards (rest new-cards))))
        (iter non-matching-cards (rest not-checked)))))
  (iter non-matching-cards (rest non-matching-cards)))


(defn run [url]
  (let [start-xpath "/html/body/div[5]/div/div/div/div[2]/button"
        obscurers-xpath "//*[@id=\"onetrust-consent-sdk\"]"]
    (doto driver
      (go url)
      (wait-exists start-xpath)
      (js-execute "arguments[0].parentElement.removeChild(arguments[0])"
                  (el->ref (query driver obscurers-xpath)))
      (click start-xpath))
      ;; (wait-exists obscurers-xpath))
    (let* [cards (get-cards)]
      (let [[pairs non-matching-cards :as _] (match-cards cards)]
        (match pairs)
        (println "begin")
        (pprint cards)
        (pprint pairs)
        (pprint non-matching-cards)
        (println)
        (println)
        (when-not (empty? non-matching-cards)
          (learn non-matching-cards)
          (run url))))))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (setup)
  (run "https://quizlet.com/528760007/match"))



(-main)
